<?php namespace StudioBosco\BackendComments\Updates;

use Schema;
use Winter\Storm\Database\Schema\Blueprint;
use Winter\Storm\Database\Updates\Migration;

class V104 extends Migration
{
    public function up()
    {
        Schema::create('studiobosco_backendcomments_reactions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('emoji')->nullable();
            $table->bigInteger('author_id')->unsigned();
            $table->bigInteger('comment_id')->unsigned();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('studiobosco_backendcomments_reactions');
    }
}
