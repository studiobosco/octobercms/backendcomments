<?php namespace StudioBosco\BackendComments\Models;

use App;
use Model;
use Lang;
use Event;
use Backend;
use BackendAuth;
use Backend\Models\User;

use StudioBosco\BackendComments\Classes\CommentMention;

/**
 * Comment Model
 */
class Comment extends Model
{
    use \Winter\Storm\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'studiobosco_backendcomments_comments';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'body',
        'author_id',
        'commentable_id',
        'commentable_type',
        'source_url',
    ];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [
        'body_formatted',
    ];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public bool $notificationsEnabled = true;

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'reactions' => [\StudioBosco\BackendComments\Models\Reaction::class, 'delete' => true,],
    ];
    public $hasOneThrough = [];
    public $hasManyThrough = [];
    public $belongsTo = [
        'author' => 'Backend\Models\User',
    ];
    public $belongsToMany = [];
    public $morphTo = [
        'commentable' => [],
    ];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function __construct(array $attriubtes = [])
    {
        $this->notificationsEnabled = array_get($attriubtes, 'notificationsEnabled', true);
        unset($attriubtes['notificationsEnabled']);
        return parent::__construct($attriubtes);
    }

    public function beforeCreate()
    {
        if (!isset($this->author_id)) {
            $this->author = BackendAuth::getUser();
        }
        if (App::runningInBackend() && !$this->source_url) {
            $this->source_url = request()->getRequestUri();
        }
    }

    public function afterSave()
    {
        // detect mentions
        if ($this->notificationsEnabled) {
            $mentions = $this->getMentions();
            $this->notifyMentionedUsers($mentions);
        }
    }

    protected function getMentionableUserNames(User $user)
    {
        $names = [trim($user->login)];

        if (trim($user->full_name)) {
            $names[] = trim($user->full_name);
        }

        if (trim($user->first_name)) {
            $names[] = trim($user->first_name);
        }

        if (trim($user->last_name)) {
            $names[] = trim($user->last_name);
        }

        return $names;
    }

    protected function getMentionsForUser(User $user)
    {
        $names = $this->getMentionableUserNames($user);
        $mentions = [];
        $body = strtolower($this->body);

        foreach($names as $name) {
            $pos = strpos($body, '@' . strtolower($name));
            if ($pos !== false) {
                $mentions[] = new CommentMention([
                    'comment' => $this,
                    'user' => $user,
                    'excerpt' => substr($body, $pos, 160) . (strlen($body) - $pos - 160 > 0 ? ' …' : ''),
                    'url' => '', // TODO: add api to get url to mention
                ]);
                break;
            }
        }

        return $mentions;
    }

    public function getMentions()
    {
        $users = User::all();
        $mentions = [];

        foreach($users as $user) {
            $userMentions = $this->getMentionsForUser($user);
            $mentions = array_merge($mentions, $userMentions);
        }

        return $mentions;
    }

    public function notifyMentionedUsers(array $mentions)
    {
        $author = $this->author;

        foreach($mentions as $mention) {
            Event::fire('studiobosco.backendcomments.mention', $mention);

            $user = $mention->user;

            if (!$user) {
                continue;
            }

            $subject = $author
            ? Lang::get('studiobosco.backendcomments::lang.mention_subject', [
                'author' => $author->full_name,
            ])
            : Lang::get('studiobosco.backendcomments::lang.mention_subject_no_author');

            $body = $mention->excerpt;
            $url = $mention->url;
            $key = implode('::', ['studiobosco.backendcomments', $author ? $author->id : '?', $user->id, $this->id]);

            Event::fire('studiobosco.backendnotifications.notify', [
                $user,
                $subject,
                $body,
                $url,
                $key,
            ]);
        }
    }

    public function canBackendUserEdit($user = null)
    {
        if (!$user) {
            $user = BackendAuth::getUser();
        }

        if ($user->is_superuser) {
            return true;
        } elseif ($this->author) {
            // fallback
            return $this->author->id === $user->id;
        }

        return false;
    }

    public function getBodyFormattedAttribute()
    {
        $body = $this->body ?? '';

        return $this->linkMentions($body);
    }

    protected function linkMentions($body)
    {
        $users = User::all();

        foreach($users as $user) {
            $names = $this->getMentionableUserNames($user);

            foreach($names as $name) {
                $marker = '@' . $name;
                $replacement = '[user-mention:' . $user->id . ']';
                $body = str_ireplace($marker, $replacement, $body);
            }

            $handle = trim($user->first_name . ' ' . $user->last_name);
            $handle = $handle ? $handle : $user->login;

            $link = '<a class="btn-link" href="' . Backend::url('backend/users/preview/' . $user->id) . '">@' . $handle . '</a>';
            $body = str_replace('[user-mention:' . $user->id . ']', $link, $body);
        }

        return $body;
    }

    public function scopeListBackend($query)
    {
        $user = BackendAuth::getUser();

        if ($user->is_superuser) {
            return $query;
        } else {
            return $query->isRelevantForUser();
        }
    }

    public function scopeIsRelevantForUser($query, $user = null)
    {
        if (!$user) {
            $user = BackendAuth::getUser();
        }

        if (!$user) {
            return $query;
        }

        $names = $this->getMentionableUserNames($user);

        $query->where(function ($query) use ($names) {
            foreach($names as $name) {
                $query->orWhere('body', 'LIKE', '%@' . $name . '%');
            }
        });

        // TODO: implement api to allow extending this scope
        return $query;
    }

    public function toggleReaction($emoji) {
        $user = BackendAuth::getUser();

        if (!$user) {
            return;
        }

        $existingReactions = $this->reactions()->where('author_id', $user->id)->get();

        // must compare using php as DB is case insensitive
        $existing = $existingReactions->where('emoji', $emoji)->first();

        if ($existing) {
            $existing->delete();
        } else {
            $this->reactions()->create([
                'author_id' => $user->id,
                'emoji' => $emoji,
            ]);
        }
    }

    public function getReactionCount($emoji)
    {
        return $existingReactions = $this->reactions->where('emoji', $emoji)->count();
    }

    public function getFormattedReactionAuthors($emoji)
    {
        $authors = [];

        foreach($this->reactions->where('emoji', $emoji) as $reaction) {
            $authors[$reaction->author_id] = $reaction->author->full_name;
        }

        return implode(', ', $authors);
    }
}
